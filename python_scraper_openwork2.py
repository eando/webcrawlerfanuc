"""
#社説スクレイピングコードをもとにオープンワークの退職検討理由をcsvにしたい
step1:page1のみ取得
step2:page2以降も取得
<dd class="article_answer">以下に退職検討理由のテキストがある。これを取得したい。

"""


import re
import sys

import requests
from bs4 import BeautifulSoup

def main():
	fetch_fanuc()

def fetch_fanuc():
	#res = requests.get('https://www.vorkers.com/company_answer.php?m_id=a0910000000FrT4&q_no=8')
	# レスポンスの HTML から BeautifulSoup オブジェクトを作る
	#soup = BeautifulSoup(res.text, 'html.parser')

	# 出力ファイル
	filename_out = 'output_fanuc.txt'
	out = open(filename_out,'w')

	# HTMLタグを除去するためにHTMLタグの正規表現を設定
	p = re.compile(r"<[^>]*?>")

	# 403エラーが出たので、ひとまずHTMLファイルを読み込む方法をとる
	filename = "openwork_source.html"
	soup = BeautifulSoup(open(filename), 'html.parser')

	# title タグの文字列を取得する
	articles = soup.find_all('dd', class_='article_answer')
	for article in articles:
		# デバッグ用に標準出力
		#print(article)

		# HTMLタグを除去
		article_notag = p.sub("",str(article))
		# 文字型に変換してファイル出力
		out.write(article_notag + '\n')

if __name__ == '__main__':
	main()

#=======以下、未使用===============
	

def fetch1(url1,num):
	#各記事のhtmlを出力するテキストファイル
	#filename = 'source_' + str(num) + '.txt'
	filename = "openwork_source.html"
	#各記事のタイトルと本文を出力するテキストファイル（アウトプット）
	filename_out = 'output_' + str(num) + '.txt'

	##引数のurl1のhtmlを取得してfilenameに格納
	#f = urlopen(url1)
	#encoding = f.info().get_content_charset(failobj="utf-8")
	#html = f.read().decode(encoding)
	#f = open(filename,'w')
	#f.write(html)
	#f.close()
	
	ld = open(filename)
	lines = ld.readlines()
	ld.close()

	out = open(filename_out,'w')
	
	#正規表現のパターンを定義
	#p：htmlタグ
	p = re.compile(r"<[^>]*?>")
	#p2：タブ
	p2 = re.compile(r"\t")

	##タイトルを検索してファイル出力
	#for line in lines:
		#if line.find("<title>") >= 0:
			##htmlタグを除去
			#hoge = p.sub("",line[:-1])
#
			##for DEBUG
			##print(hoge)
			#out.write(hoge + '\n')

	#記事本文を検索してファイル出力
	for line in lines:
        #ここを"dd[class='article_answer']"から持ってくる？
		if line.find("articleBody") >= 0:
			#htmlタグを除去
			hoge2 = p.sub("",line[:-1])

			#for DEBUG
			#print(p2.sub("",hoge2))
			#タブを除去してファイル出力
			out.write(p2.sub("",hoge2) + '\n')
	out.close()

def fetch(url):
	#print('start fetch')
	f = urlopen(url)
	encoding = f.info().get_content_charset(failobj="utf-8")
	html = f.read().decode(encoding)
	f = open('article_list.txt','w')
	f.write(html)
	f.close()

	ld = open('article_list.txt')
	lines = ld.readlines()
	ld.close()
	htmllist1 = []
	today = datetime.date.today()
	day1 = timedelta(days = 1)
	yesterday = today - day1
	yesterday = yesterday.strftime("%Y%m%d")
	#print(yesterday)

	for line in lines:
		if line.find(yesterday) >= 0:
			#print(line[:-1])
			#print(re.sub('^.*\"http','http',line[:-1]))
			#temp = re.sub('^.*\"http','http',line[:-1])
			temp = re.sub('^.*\/\/www','http://www',line[:-1])
			temp2 = re.sub('html.*$','html',temp)
			#print(temp2)
			htmllist1 = htmllist1 + [temp2]

	#print(htmllist1[0])
	#print(htmllist1[1])
	
	return htmllist1

def scrape(html):
	books = []
	print('start scrape')
	#for partial_html in re.findall(r'<a itemprop="url".*?</ur>\s*</a></li>', html, re.DOTALL):
	for partial_html in re.findall(r'<a itemprop="url"', html, re.DOTALL):
		url = re.search(r'<a itemprop="url" href="(.*?)">', partial_html).group(1)
		print(url)
		title = re.search(r'<p itemprop="name".*?</p>', partial_html).group(0)
		title = re.sub(r'<.*?>', '',title)
		title = unescape(title)
		print(title)
		
		books.append({'url':url, 'title':title})
	
	print('end scrape')
	return books


def save(db_path, books):
	conn = sqlite3.connect(db_path)

	c = conn.cursor()
	c.execute('DROP TABLE IF EXISTS books')
	c.execute('''
	    CREATE TABLE books(
			tite text,
			url text
    	)
	''')

	c.executemany('INSERT INTO books VALUES (:title, :url)', books)

	conn.commit()
	conn.close()

